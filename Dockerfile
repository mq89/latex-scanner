FROM python:3.8.16-slim
LABEL maintainer="mh@0x25.net"

WORKDIR /usr/src/app

COPY latex-scanner.py requirements.txt LICENSE ./
COPY texutils/ texutils/
RUN pip install -r requirements.txt

ENV PATH="/usr/src/app:${PATH}"

CMD ["latex-scanner.py"]
